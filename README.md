connect 6 is a simple Tic-tac-toe game, but on a bigger board (19x19 by default) and requires 6 figures in a line to win. 

Players input their moves into the game by typing in coordinates of place where they want to put their figure.
In the 1st round player chooses only one place to put his figure, but during next rounds each player puts 2 figures on the board.

Coordinates for a move are as following:
Ab for the 1st move.
AbCd for next moves.
Size of letters do count. 1st coordinate determines X location, and the second one Y location.
The game recognizes illegal input and doesnt accept it, asking player to type in a correct input. It will keep asking until valid input is given.

The 1st player has a figure 'X' and the second 'O'.

If the board runs out of free spaces the game ends in a draw.


Project received 19/20 points, with 10/10 for correctness and 9/10 for style of code.

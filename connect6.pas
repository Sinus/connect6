{Author: Marcin Dominiak}
program connect6;

const
	const_sizeX = 19;
	const_sizeY = 19; 
	const_winNumber = 6;
type 
	gameBoard = array[0..2*const_sizeY,0..2*const_sizeX] of integer;
var 
	board,rightRotatedBoard,leftRotatedBoard:gameBoard;
	freeSpaces:integer; {unused spaces in gameboard}
	roundCount:integer;
	currentPlayer:boolean; {false - O, true - X}
	playerMove:string; {input from player}

procedure writeBoard(); {prints gameboard}
var 
	i,j,sizeX,sizeY:integer;
	idX,idY:char;
begin
	sizeX:=const_sizeX;
	sizeY:=const_sizeY;
	idY:='a';
	for i:=0 to sizeY-2 do
		inc(idY);
	idX:='A';

	for i:=0 to (sizeX*2)-1 do
		write('-');
	writeln('+');

	for i:=0 to sizeY-1 do
	begin
		write(idY);
		dec(idY);
		for j:=0 to sizeX-1 do
		begin

			if (board[i,j]=0) then
				write ('.')
			else if (board[i,j]=1) then
				write ('X')
			else
				write('O');
			if j<>sizeX-1 then
				write(' ');
		end;
		writeln('|');
	end;

	write(' ');
	for i:=0 to sizeX-1 do
	begin
		write(idX);
		if i<>sizeX-1 then
			write(' ');
		inc(idX);
	end;
	writeln('|');
end;

procedure draw(); {calls a draw and stops the program}
begin
	writeBoard();
	writeln('remis');
	halt(0);
end;

procedure XWins(); {calls a win by player X and stops the program}
begin
	writeBoard();
	writeln('wygral X');
	halt(0);
end;

procedure OWins(); {call a win by player O and stops the program}
begin
	writeBoard();
	writeln('wygral O');
	halt(0);
end;

procedure checkWinLine(const id:integer;const horizontal:boolean;currentBoard:gameBoard;const n:integer); {id - identifier for currentl checked player, (X-1, O--1), horizontal=1 - colums, 0 - rows}
var
	i,j,counter:integer;
begin
	i:=0;
	j:=0;


	while i<=n do
	begin
		counter:=0;
		j:=0;
		while j<=const_sizeY do
		begin
			if horizontal=false then
			begin
				if currentBoard[i,j]=id then
					inc(counter)
				else
					counter:=0;
			end
			else
			begin
				if currentBoard[j,i]=id then
					inc(counter)
				else
					counter:=0;
			end;
			if counter>=const_winNumber then
			begin
				if id=1 then
					XWins()
				else
					OWins();
			end;
			inc(j);
		end;
		inc(i);
	end;
end;

function calculateY(const y :char):integer; {change y coordinate from a char to integer}
var
	sizeY:integer;
	temp:char;
begin
	temp:=chr(96);
	sizeY:=const_sizeY;
	calculateY:=sizeY;
	while (temp<y) do
	begin
		inc(temp);
		calculateY:=calculateY-1;
	end;
end;

function min(const a,b:integer):integer; {returns the smaller value of 2}
begin
	if a>=b then
		min:=a
	else
		min:=b;
end;

procedure doMove(var x1,y1,x2,y2:integer); {edits gamboard to match players moves}
var
	i,j,rx1,ry1,ly1,lx1:integer;
begin
	if roundCount>=2 then
		freeSpaces:=freeSpaces-2
	else
		freeSpaces:=freeSpaces-1;
		
	if currentPlayer=true then
	begin
		board[y1,x1]:=1;
		if roundCount>1 then
			board[y2,x2]:=1;
	end
	else
	begin
		board[y1,x1]:=-1;
		if roundCount>1 then
			board[y2,x2]:=-1;
	end;

	for i:=0 to const_sizeX do
	begin
		for j:=0 to const_sizeY do
		begin
			lx1:=i+j;
			ly1:=min(j,const_sizeY-i);
			leftRotatedBoard[ly1,lx1]:=board[j,i];

			rx1:=j+const_sizeY-i;
			ry1:=min(i,j);
			rightRotatedBoard[ry1,rx1]:=board[j,i];
		end;
	end;
end;


function validateMove():boolean; {check if input from player is corret. If it is - we do the move}
var
	x1,x2,y1,y2:integer;
	y:char;
begin
	validateMove:=false;
	if freeSpaces<=0 then
		draw();
	if length(playerMove)=0 then
		halt(0);
	if (length(playerMove)=4) and (roundCount>=2) then
	begin
		x1:=ord(playerMove[1])-65;
		x2:=ord(playerMove[3])-65;
		y:=playerMove[2];
		y1:=calculateY(y);	{changing input from chars to integers}
		y:=playerMove[4];
		y2:=calculateY(y);
		if (y1<0) or (y2<0) or (x1<0) or (x2<0) or (y1>=const_sizeY) or (y2>=const_sizeY) or (x1>=const_sizeX) or (x2>=const_sizeX) or ((x2=x1) and (y1=y2)) then
			validateMove:=false
		else if (board[y1,x1]=0) and (board[y2,x2]=0) then
		begin
			validateMove:=true;
			doMove(x1,y1,x2,y2);
		end;
	end;

	if (length(playerMove)=2) and (roundCount=1) then
	begin
		x1:=ord(playerMove[1])-65;
		y:=playerMove[2];
		y1:=calculateY(y);
		if (y1<0) or (y1>=const_sizeY) or (x1<0) or (x1>=const_sizeX) then
			validateMove:=false
		else if (board[y1,x1]=0) then
			begin
				validateMove:=true;
				doMove(x1,y1,x1,y1);
			end;
	end;
end;

procedure makeBoard();	{prepares gamboard - 0 in each cell of array}
var
	i,j:integer;
begin
	freeSpaces:=const_sizeX*const_sizeY;
	for i:=0 to const_SizeX do
	begin
		for j:=0 to const_SizeY do
			board[i,j]:=0;
	end;

	for i:=0 to const_SizeX*2 do
	begin
		for j:=0 to const_SizeY*2 do
			rightRotatedBoard[i,j]:=0;
	end;
end;

begin {main loop - read input, validate, check if anyone won}
	roundCount:=1;
	currentPlayer:=true;
	makeBoard();
	playerMove:='';
	while roundCount>=1 do 
	begin
		writeBoard();
		if currentPlayer=true then
			writeln('gracz X')
		else
			writeln('gracz O');
		readln(playerMove);
		if validateMove()=true then
		begin
			if freeSpaces<=0 then
				draw();	
			checkWinLine(1,false,board,const_sizeY);
			checkWinLine(1,true,board,const_sizeY);
			checkWinLine(1,false,rightRotatedBoard,const_sizeY*2-1);		
			checkWinLine(1,true,rightRotatedBoard,const_sizeY*2-1);
			checkWinLine(1,false,leftRotatedBoard,const_sizeY*2-1);
			checkWinLine(1,true,leftRotatedBoard,const_sizeY*2-1);
			checkWinLine(-1,false,board,const_sizeY);
			checkWinLine(-1,true,board,const_sizeY);
			checkWinLine(-1,false,rightRotatedBoard,const_sizeY*2-1);
			checkWinLine(-1,true,rightRotatedBoard,const_sizeY*2-1);
			checkWinLine(-1,false,leftRotatedBoard,const_sizeY*2-1);
			checkWinLine(-1,true,leftRotatedBoard,const_sizeY*2-1);
			currentPlayer:=not currentPlayer; 
			inc(roundCount);
		end;
	end;
end.	 
